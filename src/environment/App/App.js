import React from 'react';
import MainView from 'lib/MainView';

const App = () => <MainView />;

export default App;

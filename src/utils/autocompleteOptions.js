/* eslint-disable prettier/prettier */
const autocompleteOptions = {
  cities: [
    {label: 'Addis Abeba'},
    {label: 'Cairo'},
    {label: 'Cape Town'},
    {label: 'Darfur'},
  ],
  weights: [{label: '<1 kg'}, {label: '1-5 kg'}, {label: '>5 kg'}],
  cargoType: [
    {label: 'Recorded Delivery'},
    {label: 'Weapons'},
    {label: 'Live Animals'},
    {label: 'Cautious parcels'},
    {label: 'Refrigerated goods'},
  ],
};

export default autocompleteOptions;

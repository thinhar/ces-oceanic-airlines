import React from 'react';
import ParcelsColumn from 'lib/ParcelsColumn';

import classes from './ParcelsColumnContainer.module.css';

const ParcelsColumnContainer = () => (
  <div className={classes.parcelsColumnContainer}>
    <ParcelsColumn parcelsColumnType="dispatched" />
    <ParcelsColumn parcelsColumnType="completed" />
  </div>
);

ParcelsColumnContainer.propTypes = {};

export default ParcelsColumnContainer;

import React, {useState} from 'react';
import PropTypes from 'prop-types';

import ParcelDataInput from 'lib/ParcelDataInput';
import DeliveryOptionSelection from 'lib/DeliveryOptionSelection';

import CloseIcon from '@mui/icons-material/Close';

import classes from './AddNewParcelModal.module.css';

const AddNewParcelModal = props => {
  const {closeModal} = props;

  const [isQuoted, setIsQuoted] = useState(false);
  const [isAwaiting, setIsAwaiting] = useState(false);

  const [origin, setOrigin] = useState('');
  const [destination, setDestination] = useState('');
  const [weight, setWeight] = useState('');
  const [cargoType, setCargoType] = useState('');
  const [depth, setDepth] = useState('');
  const [height, setHeight] = useState('');
  const [bredth, setBredth] = useState('');

  const originChangeCallback = event => {
    setOrigin(event?.target?.firstChild?.data);
  };

  const destinationChangeCallback = event => {
    setDestination(event?.target?.firstChild?.data);
  };

  const weightChangeCallback = event => {
    setWeight(event.target.value);
  };

  const cargoTypeChangeCallback = event => {
    setCargoType(event?.target?.firstChild?.data);
  };

  const depthChangeCallback = event => {
    setDepth(event.target.value);
  };
  const heightChangeCallback = event => {
    setHeight(event.target.value);
  };
  const bredthChangeCallback = event => {
    setBredth(event.target.value);
  };

  return (
    <>
      <div className={classes.windowBlur} />
      <div className={classes.modalWindow}>
        {!isQuoted && !isAwaiting && (
          <ParcelDataInput
            origin={origin}
            destination={destination}
            weight={weight}
            cargoType={cargoType}
            depth={depth}
            height={height}
            bredth={bredth}
            originChangeCallback={originChangeCallback}
            destinationChangeCallback={destinationChangeCallback}
            weightChangeCallback={weightChangeCallback}
            cargoTypeChangeCallback={cargoTypeChangeCallback}
            handleNext={() => {
              setIsAwaiting(true);
              setTimeout(() => {
                setIsAwaiting(false);
                setIsQuoted(true);
              }, 5000);
            }}
            depthChangeCallback={depthChangeCallback}
            heightChangeCallback={heightChangeCallback}
            bredthChangeCallback={bredthChangeCallback}
          />
        )}
        {!isQuoted && isAwaiting && (
          <div className={classes.modalWindowAwaitingContainer}>
            <div className={classes.modalWindowAwaitingText}>
              Calculating available routes
            </div>
            <span className={classes.modalWindowLoader} />
          </div>
        )}
        {isQuoted && (
          <DeliveryOptionSelection
            origin={origin}
            destination={destination}
            weight={weight}
            cargoType={cargoType}
            depth={depth}
            height={height}
            bredth={bredth}
            back={() => setIsQuoted(false)}
          />
        )}
        {!isAwaiting && (
          <button
            type="button"
            onClick={closeModal}
            className={classes.closeModalWindow}>
            <CloseIcon sx={{fontSize: 35, mt: '5px'}} />
          </button>
        )}
      </div>
    </>
  );
};

AddNewParcelModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
};

export default AddNewParcelModal;

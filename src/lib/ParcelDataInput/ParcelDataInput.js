/* eslint-disable fp/no-mutation */
import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';

import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

import autocompleteOptions from 'utils/autocompleteOptions';

import classes from './ParcelDataInput.module.css';

let map;

const ParcelDataInput = props => {
  const {
    origin,
    destination,
    weight,
    cargoType,
    depth,
    height,
    bredth,
    originChangeCallback,
    destinationChangeCallback,
    weightChangeCallback,
    cargoTypeChangeCallback,
    handleNext,
    depthChangeCallback,
    heightChangeCallback,
    bredthChangeCallback,
  } = props;

  const [isFormValid, setIsFormValid] = useState(false);

  const handleOriginChange = event => {
    originChangeCallback(event);
    L.circle([2.616667, 16.1], {
      color: 'red',
      fillColor: '#f03',
      fillOpacity: 0.5,
      radius: 100000,
    }).addTo(map);
  };

  const handleDestinationChange = event => {
    destinationChangeCallback(event);
    L.circle([22.616667, 6.1], {
      color: 'green',
      fillColor: '#f03',
      fillOpacity: 0.5,
      radius: 100000,
    }).addTo(map);
  };

  const handleWeightChange = event => {
    weightChangeCallback(event);
  };

  const handleCargoTypeChange = event => {
    cargoTypeChangeCallback(event);
  };

  useEffect(() => {
    map = L.map('map', {
      center: [2.616667, 16.1],
      zoom: 2,
    });

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© OpenStreetMap',
    }).addTo(map);
  }, []);

  const selectOptions = type => {
    switch (type) {
      case 'origin':
      case 'destination':
        return autocompleteOptions.cities;
      case 'weight':
        return autocompleteOptions.weights;
      case 'cargo type':
        return autocompleteOptions.cargoType;
      default:
        return [];
    }
  };

  const validateInput = () => {
    if (
      origin &&
      destination &&
      weight > 0 &&
      cargoType &&
      depth > 0 &&
      height > 0 &&
      bredth > 0 &&
      origin !== destination
    ) {
      return true;
    }
    return false;
  };

  useEffect(() => {
    setIsFormValid(validateInput());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [origin, destination, weight, cargoType, depth, height, bredth]);

  return (
    <>
      <div className={classes.parcelInputHeader}>Create new parcel</div>
      <div className={classes.parcelInputContent}>
        <div className={classes.parcelInputNonMapColumn}>
          <div className={classes.parcelInputSingle}>
            <div className={classes.parcelCardFieldLabel}>From:</div>
            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={selectOptions('origin')}
              sx={{
                width: '20vw',
                bgcolor: '#fff',
                mt: '10px',
                borderRadius: '4px',
              }}
              value={origin}
              size="small"
              onChange={handleOriginChange}
              renderInput={params => <TextField {...params} />}
            />
          </div>
          <div className={classes.parcelInputSingle}>
            <div className={classes.parcelCardFieldLabel}>To:</div>
            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={selectOptions('destination')}
              sx={{
                width: '20vw',
                bgcolor: '#fff',
                mt: '10px',
                borderRadius: '4px',
              }}
              value={destination}
              size="small"
              onChange={handleDestinationChange}
              renderInput={params => <TextField {...params} />}
            />
          </div>
          <div className={classes.parcelInputSingle}>
            <div className={classes.parcelCardFieldLabel}>Weight:</div>
            <TextField
              size="small"
              type="number"
              sx={{
                width: '20vw',
                bgcolor: '#fff',
                mt: '10px',
                borderRadius: '4px',
              }}
              value={weight}
              onChange={handleWeightChange}
            />
          </div>
          <div className={classes.parcelInputSingle}>
            <div className={classes.parcelCardFieldLabel}>Cargo Type:</div>
            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={selectOptions('cargo type')}
              sx={{
                width: '20vw',
                bgcolor: '#fff',
                mt: '10px',
                borderRadius: '4px',
              }}
              value={cargoType}
              size="small"
              onChange={handleCargoTypeChange}
              renderInput={params => <TextField {...params} />}
            />
          </div>
        </div>
        <div className={classes.parcelInputMapColumn}>
          <div className={classes.parcelInputSingle}>
            <div className={classes.parcelCardFieldLabel}>Dimensions:</div>
            <div className={classes.parcelCardDimensionsInput}>
              <TextField
                size="small"
                type="number"
                sx={{
                  width: '6vw',
                  bgcolor: '#fff',
                  mt: '10px',
                  borderRadius: '4px',
                }}
                value={depth}
                onChange={depthChangeCallback}
              />
              <span className={classes.parcelCardDimensionsDelimiter}>x</span>
              <TextField
                size="small"
                type="number"
                sx={{
                  width: '6vw',
                  bgcolor: '#fff',
                  mt: '10px',
                  borderRadius: '4px',
                }}
                value={height}
                onChange={heightChangeCallback}
              />
              <span className={classes.parcelCardDimensionsDelimiter}>x</span>
              <TextField
                size="small"
                type="number"
                sx={{
                  width: '6vw',
                  bgcolor: '#fff',
                  mt: '10px',
                  borderRadius: '4px',
                }}
                value={bredth}
                onChange={bredthChangeCallback}
              />
            </div>
          </div>
          <div id="map" className={classes.parcelInputMap} />
        </div>
      </div>
      <button
        type="button"
        className={`${classes.parcelInputNextButton} ${
          !isFormValid ? classes.parcelInputNextButtonDisabled : ''
        }`}
        onClick={handleNext}
        disabled={!isFormValid}>
        Next
      </button>
    </>
  );
};

ParcelDataInput.propTypes = {
  origin: PropTypes.string.isRequired,
  destination: PropTypes.string.isRequired,
  weight: PropTypes.number.isRequired,
  cargoType: PropTypes.string.isRequired,
  depth: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  bredth: PropTypes.number.isRequired,
  originChangeCallback: PropTypes.func.isRequired,
  destinationChangeCallback: PropTypes.func.isRequired,
  weightChangeCallback: PropTypes.func.isRequired,
  cargoTypeChangeCallback: PropTypes.func.isRequired,
  handleNext: PropTypes.func.isRequired,
  depthChangeCallback: PropTypes.func.isRequired,
  heightChangeCallback: PropTypes.func.isRequired,
  bredthChangeCallback: PropTypes.func.isRequired,
};

export default ParcelDataInput;

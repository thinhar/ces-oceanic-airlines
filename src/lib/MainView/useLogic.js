import {useState} from 'react';

const useLogic = initialState => {
  const [parcelModalVisible, seParcelModalVisible] = useState(initialState);
  return {
    parcelModalVisible,
    openParcelModalVisible: () => {
      seParcelModalVisible(true);
    },
    closeParcelModalVisible: () => {
      seParcelModalVisible(false);
    },
  };
};

export default useLogic;

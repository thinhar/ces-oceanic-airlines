import React from 'react';
import Navbar from 'lib/Navbar';
import ParcelsColumnContainer from 'lib/ParcelsColumnContainer';
import AddNewParcelModal from 'lib/AddNewParcelModal';
import useLogic from './useLogic';
import classes from './MainView.module.css';

const MainView = () => {
  const {parcelModalVisible, openParcelModalVisible, closeParcelModalVisible} =
    useLogic(false);

  return (
    <div className={classes.componentsContainer}>
      <Navbar handleAddNewParcel={openParcelModalVisible} />
      <ParcelsColumnContainer />
      {parcelModalVisible && (
        <AddNewParcelModal closeModal={closeParcelModalVisible} />
      )}
    </div>
  );
};

MainView.propTypes = {
  /**
   * Define initial value for the Counter
   */
};

export default MainView;

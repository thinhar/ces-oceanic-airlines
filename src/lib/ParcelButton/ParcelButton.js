import React from 'react';
import PropTypes from 'prop-types';
// import useLogic from './useLogic';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import classes from './ParcelButton.module.css';

const ParcelButton = props => {
  const {handleAddNewParcel} = props;

  return (
    <button
      className={classes.button}
      type="button"
      onClick={handleAddNewParcel}>
      <div className={classes.buttonContent}>
        <AddCircleOutlineIcon sx={{fontSize: 35}} />
        <span className={classes.buttonText}>Add new parcel</span>
      </div>
    </button>
  );
};

ParcelButton.propTypes = {
  /**
   * Define initial value for the Counter
   */
  handleAddNewParcel: PropTypes.func.isRequired,
};

export default ParcelButton;

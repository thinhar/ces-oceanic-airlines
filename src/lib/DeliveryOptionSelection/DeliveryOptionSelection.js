import React, {useState} from 'react';
import PropTypes from 'prop-types';

import TextField from '@mui/material/TextField';

import classes from './DeliveryOptionSelection.module.css';

const DeliveryOptionSelection = props => {
  const {origin, destination, weight, cargoType, depth, height, bredth, back} =
    props;

  const [selectedOption, setSelectedOption] = useState(1);

  const optionsAmount = 2;
  const firstButtonClass =
    selectedOption === 1 ? classes.deliveryOptionSelectedOption : '';
  const secondButtonClass =
    selectedOption === 2 ? classes.deliveryOptionSelectedOption : '';
  const thirdButtonClass =
    selectedOption === 3 ? classes.deliveryOptionSelectedOption : '';

  return (
    <>
      <div className={classes.deliveryOptionHeader}>Select delivery option</div>
      <div className={classes.deliveryOptionContent}>
        <div className={classes.deliveryOptionContentRow}>
          <div className={classes.deliveryOptionContentRowColumn}>
            <div className={classes.deliveryOptionFieldContent}>
              <div className={classes.deliveryOptionFieldLabel}>To:</div>
              <TextField
                size="small"
                sx={{
                  width: '18vw',
                  bgcolor: '#fff',
                  mt: '10px',
                  borderRadius: '4px',
                }}
                value={origin}
              />
            </div>
            <div className={classes.deliveryOptionFieldContent}>
              <div className={classes.deliveryOptionFieldLabel}>From:</div>
              <TextField
                size="small"
                sx={{
                  width: '18vw',
                  bgcolor: '#fff',
                  mt: '10px',
                  borderRadius: '4px',
                }}
                value={destination}
              />
            </div>
            <div className={classes.deliveryOptionFieldContent}>
              <div className={classes.deliveryOptionFieldLabel}>Weight:</div>
              <TextField
                size="small"
                sx={{
                  width: '18vw',
                  bgcolor: '#fff',
                  mt: '10px',
                  borderRadius: '4px',
                }}
                value={weight}
              />
            </div>
          </div>
          <div className={classes.deliveryOptionContentRowColumn}>
            {optionsAmount === 1 && (
              <div className={classes.deliveryOptionTableHeader}>
                <button
                  type="button"
                  className={classes.deliveryOptionSoloButton}>
                  Option #1
                </button>
              </div>
            )}
            {optionsAmount === 2 && (
              <div className={classes.deliveryOptionTableHeader}>
                <button
                  type="button"
                  className={`${classes.deliveryOptionDualButtons} ${firstButtonClass}`}
                  onClick={() => {
                    setSelectedOption(1);
                  }}>
                  Option #1
                </button>
                <div className={classes.deliveryOptionVerticalLine} />
                <button
                  type="button"
                  className={`${classes.deliveryOptionDualButtons} ${secondButtonClass}`}
                  onClick={() => {
                    setSelectedOption(2);
                  }}>
                  Option #2
                </button>
              </div>
            )}
            {optionsAmount === 3 && (
              <div className={classes.deliveryOptionTableHeader}>
                <button
                  type="button"
                  className={`${classes.deliveryOptionTripleButtons} ${firstButtonClass}`}
                  onClick={() => {
                    setSelectedOption(1);
                  }}>
                  Option #1
                </button>
                <div className={classes.deliveryOptionVerticalLine} />
                <button
                  type="button"
                  className={`${classes.deliveryOptionTripleButtons} ${secondButtonClass}`}
                  onClick={() => {
                    setSelectedOption(2);
                  }}>
                  Option #2
                </button>
                <div className={classes.deliveryOptionVerticalLine} />
                <button
                  type="button"
                  className={`${classes.deliveryOptionTripleButtons} ${thirdButtonClass}`}
                  onClick={() => {
                    setSelectedOption(3);
                  }}>
                  Option #3
                </button>
              </div>
            )}
            <div className={classes.deliveryOptionTableContent}>
              <div className={classes.deliveryOptionContentRowSmall}>
                <div className={classes.deliveryOptionFieldContentSmall}>
                  <div className={classes.deliveryOptionFieldLabel}>Price:</div>
                  <TextField
                    size="small"
                    sx={{
                      width: '6vw',
                      bgcolor: '#fff',
                      mt: '10px',
                      borderRadius: '4px',
                    }}
                    value="80"
                  />
                </div>
                <div className={classes.deliveryOptionFieldContentSmall}>
                  <div className={classes.deliveryOptionFieldLabel}>
                    Currency:
                  </div>
                  <TextField
                    size="small"
                    sx={{
                      width: '6vw',
                      bgcolor: '#fff',
                      mt: '10px',
                      borderRadius: '4px',
                    }}
                    value="USD"
                  />
                </div>
              </div>
              <div className={classes.deliveryOptionContentRowSmall}>
                <div className={classes.deliveryOptionFieldContentSmall}>
                  <div className={classes.deliveryOptionFieldLabel}>
                    Estimated time of delivery:
                  </div>
                  <TextField
                    size="small"
                    sx={{
                      width: '15vw',
                      bgcolor: '#fff',
                      mt: '10px',
                      borderRadius: '4px',
                    }}
                    value="01/01/2022 22:00"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.deliveryOptionContentRow}>
          <div>
            <div className={classes.deliveryOptionFieldLabel}>Dimensions:</div>
            <div className={classes.deliveryOptionDimensionsInput}>
              <TextField
                size="small"
                sx={{
                  width: '6vw',
                  bgcolor: '#fff',
                  mt: '10px',
                  borderRadius: '4px',
                }}
                value={depth}
              />
              <span className={classes.deliveryOptionDimensionsDelimiter}>
                x
              </span>
              <TextField
                size="small"
                sx={{
                  width: '6vw',
                  bgcolor: '#fff',
                  mt: '10px',
                  borderRadius: '4px',
                }}
                value={height}
              />
              <span className={classes.deliveryOptionDimensionsDelimiter}>
                x
              </span>
              <TextField
                size="small"
                sx={{
                  width: '6vw',
                  bgcolor: '#fff',
                  mt: '10px',
                  borderRadius: '4px',
                }}
                value={bredth}
              />
            </div>
          </div>
          <div className={classes.deliveryOptionMarginLeft}>
            <div className={classes.deliveryOptionFieldLabel}>Cargo type:</div>
            <TextField
              size="small"
              sx={{
                width: '18vw',
                bgcolor: '#fff',
                mt: '10px',
                borderRadius: '4px',
              }}
              value={cargoType}
            />
          </div>
        </div>
      </div>
      <div className={classes.deliveryOptionButtonContainer}>
        <button type="button" className={classes.deliveryOptionNextButton}>
          Submit
        </button>

        <button
          type="button"
          className={classes.deliveryOptionBackButton}
          onClick={back}>
          Back
        </button>
      </div>
    </>
  );
};

DeliveryOptionSelection.propTypes = {
  origin: PropTypes.string.isRequired,
  destination: PropTypes.string.isRequired,
  weight: PropTypes.number.isRequired,
  cargoType: PropTypes.string.isRequired,
  depth: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  bredth: PropTypes.number.isRequired,
  back: PropTypes.func.isRequired,
};

export default DeliveryOptionSelection;

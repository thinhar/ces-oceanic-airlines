import {useState} from 'react';

const useLogic = initialState => {
  const [filteringVisible, setFilteringVisible] = useState(initialState);
  return {
    filteringVisible,
    toggleFilteringVisible: () => {
      setFilteringVisible(!filteringVisible);
    },
  };
};

export default useLogic;

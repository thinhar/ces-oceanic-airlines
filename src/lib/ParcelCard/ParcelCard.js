import React from 'react';
import PropTypes from 'prop-types';

import ArchiveIcon from '@mui/icons-material/Archive';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import TextField from '@mui/material/TextField';

import classes from './ParcelCard.module.css';

const ParcelCard = props => {
  const {
    parcelType,
    cargoType,
    origin,
    destination,
    price,
    depth,
    height,
    bredth,
    weight,
    departure,
    arrival,
  } = props;

  return (
    <div className={classes.parcelCard}>
      <div className={classes.parcelsCardMultiRow}>
        <div className={classes.parcelCardIcon}>
          {parcelType === 'dispatched' ? (
            <LocalShippingIcon sx={{color: '#fff', fontSize: 80}} />
          ) : (
            <ArchiveIcon sx={{fontSize: 80}} />
          )}
        </div>
        <div className={classes.parcelCardFieldPair}>
          <div className={classes.parcelCardRow}>
            <div className={classes.pracelCardFieldSmall}>
              <div className={classes.parcelCardFieldLabel}>Cargo type:</div>
              <TextField
                sx={{bgcolor: '#fff', borderRadius: '4px', width: '100%'}}
                value={cargoType}
                size="small"
              />
            </div>
            <div className={classes.pracelCardFieldMedium}>
              <div className={classes.parcelCardFieldLabel}>Destination:</div>
              <TextField
                sx={{bgcolor: '#fff', borderRadius: '4px', width: '100%'}}
                value={destination}
                size="small"
              />
            </div>
          </div>
          <div className={classes.parcelCardRow}>
            <div className={classes.pracelCardFieldSmall}>
              <div className={classes.parcelCardFieldLabel}>Price:</div>
              <TextField
                sx={{bgcolor: '#fff', borderRadius: '4px', width: '100%'}}
                value={`${price} USD`}
                size="small"
              />
            </div>
            <div className={classes.pracelCardFieldMedium}>
              <div className={classes.parcelCardFieldLabel}>Origin:</div>
              <TextField
                sx={{bgcolor: '#fff', borderRadius: '4px', width: '100%'}}
                value={origin}
                size="small"
              />
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className={classes.parcelCardRow}>
          <div className={classes.pracelCardFieldMedium}>
            <div className={classes.parcelCardFieldLabel}>Dimensions:</div>
            <div className={classes.parcelCardDiametersField}>
              <TextField
                sx={{bgcolor: '#fff', borderRadius: '4px', width: '30%'}}
                value={depth}
                size="small"
              />
              <span className={classes.parcelCardDiametersDelimiter}>x</span>
              <TextField
                sx={{bgcolor: '#fff', borderRadius: '4px', width: '30%'}}
                value={height}
                size="small"
              />
              <span className={classes.parcelCardDiametersDelimiter}>x</span>
              <TextField
                sx={{bgcolor: '#fff', borderRadius: '4px', width: '30%'}}
                value={bredth}
                size="small"
              />
            </div>
          </div>
          <div className={classes.pracelCardFieldSmall}>
            <div className={classes.parcelCardFieldLabel}>Weight:</div>
            <TextField
              sx={{bgcolor: '#fff', borderRadius: '4px', width: '100%'}}
              value={weight}
              size="small"
            />
          </div>
        </div>
      </div>
      <div>
        <div className={classes.parcelCardRow}>
          <div className={classes.pracelCardFieldDate}>
            <div className={classes.parcelCardFieldLabel}>Departed at:</div>
            <TextField
              sx={{bgcolor: '#fff', borderRadius: '4px', width: '100%'}}
              value={departure}
              size="small"
            />
          </div>
          <div className={classes.pracelCardFieldDate}>
            <div className={classes.parcelCardFieldLabel}>
              {parcelType === 'dispatched'
                ? 'Estimated time of arrival:'
                : 'Arrived at:'}
            </div>
            <TextField
              sx={{bgcolor: '#fff', borderRadius: '4px', width: '100%'}}
              value={arrival}
              size="small"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

ParcelCard.propTypes = {
  parcelType: PropTypes.string.isRequired,
  cargoType: PropTypes.string.isRequired,
  origin: PropTypes.string.isRequired,
  destination: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  depth: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  bredth: PropTypes.number.isRequired,
  weight: PropTypes.string.isRequired,
  departure: PropTypes.string.isRequired,
  arrival: PropTypes.string.isRequired,
};

export default ParcelCard;

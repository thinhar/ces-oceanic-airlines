import React from 'react';
import PropTypes from 'prop-types';
// import useLogic from './useLogic';
import ParcelButton from 'lib/ParcelButton';
import classes from './Navbar.module.css';
import oceanicLogo from '../../assets/oceanic-airlines-logo.png';

const Navbar = props => {
  const {handleAddNewParcel} = props;

  return (
    <div className={classes.verticalNav}>
      <img src={oceanicLogo} alt="oceanic airlines logo" />
      <ParcelButton handleAddNewParcel={handleAddNewParcel} />
    </div>
  );
};

Navbar.propTypes = {
  /**
   * Define initial value for the Counter
   */
  handleAddNewParcel: PropTypes.func.isRequired,
};

export default Navbar;

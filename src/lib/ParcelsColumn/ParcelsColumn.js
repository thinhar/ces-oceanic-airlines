import React, {useState} from 'react';
import PropTypes from 'prop-types';

import FilterAltIcon from '@mui/icons-material/FilterAlt';
import FilterAltOffIcon from '@mui/icons-material/FilterAltOff';
import SearchIcon from '@mui/icons-material/Search';

import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

import {AdapterMoment} from '@mui/x-date-pickers/AdapterMoment';
import {LocalizationProvider} from '@mui/x-date-pickers/LocalizationProvider';
import {DatePicker} from '@mui/x-date-pickers/DatePicker';

import autocompleteOptions from 'utils/autocompleteOptions';
import ParcelCard from 'lib/ParcelCard';
import classes from './ParcelsColumn.module.css';

// TODO: Fix date picker styling

const sampleParcels = [
  {
    cargoType: 'Weapons',
    origin: 'Addis Abeba',
    destination: 'Darfur',
    price: '160',
    depth: 20,
    height: 30,
    bredth: 40,
    weight: '>5 kg',
    departure: '01/01/2022 10:00',
    arrival: '01/01/2022 18:00',
  },
  {
    cargoType: 'Refrigerated goods',
    origin: 'Cairo',
    destination: 'Darfur',
    price: '80',
    depth: 20,
    height: 30,
    bredth: 40,
    weight: '>5 kg',
    departure: '01/01/2022 10:00',
    arrival: '01/01/2022 18:00',
  },
  {
    cargoType: 'Cautious parcels',
    origin: 'Addis Abeba',
    destination: 'Cape Town',
    price: '80',
    depth: 20,
    height: 30,
    bredth: 40,
    weight: '1-5 kg',
    departure: '01/01/2022 10:00',
    arrival: '01/01/2022 18:00',
  },
  {
    cargoType: 'Cautious parcels',
    origin: 'Cairo',
    destination: 'Cape Town',
    price: '80',
    depth: 20,
    height: 30,
    bredth: 40,
    weight: '<1 kg',
    departure: '01/01/2022 10:00',
    arrival: '01/01/2022 18:00',
  },
];

const ParcelsColumn = props => {
  const {parcelsColumnType} = props;

  const parcelsList = sampleParcels;
  const [renderedParcelsList, setRenderedParcelsList] = useState(sampleParcels);
  const [filteringVisible, setFilteringVisible] = useState(false);
  const [filterBy, setFilterBy] = useState('');
  const [filterValue, setFilterValue] = useState('');
  const [filterDatetimeValue, setFilterDatetimeValue] = useState('');

  const handleChange = event => {
    setFilterBy(event.target.value);
    setFilterValue('');
    setRenderedParcelsList(parcelsList);
  };

  const handleFilterValueChange = event => {
    setFilterValue(event?.target?.firstChild?.data);
    if (event?.target?.firstChild?.data) {
      setRenderedParcelsList(parcelsList);
    }
  };

  const handleDateValueChange = value => {
    setFilterDatetimeValue(value);
  };

  const parcelsColumnHeaderMap = {
    dispatched: 'Dispatched parcels:',
    completed: 'Completed parcels:',
  };

  const arrivalFieldLabelMap = {
    dispatched: 'ETA',
    completed: 'Arrived',
  };

  const filteringValues = () => {
    switch (filterBy) {
      case 'origin':
      case 'destination':
        return autocompleteOptions.cities;
      case 'weight':
        return autocompleteOptions.weights;
      case 'cargoType':
        return autocompleteOptions.cargoType;
      default:
        return [];
    }
  };

  const toggleFilteringVisible = () => {
    setFilteringVisible(!filteringVisible);
    setRenderedParcelsList(parcelsList);
  };

  const filterParcels = () => {
    const parcels = [...parcelsList].filter(
      parcel => parcel[filterBy] === filterValue
    );
    setRenderedParcelsList(parcels);
  };

  const createFilterForm = () => (
    <FormControl sx={{m: 1}}>
      <div className={classes.parcelsColumnFiltering}>
        <div>
          <FormHelperText>
            <span className={classes.parcelsColumnFilterLabel}>Filter by:</span>
          </FormHelperText>
          <Select
            sx={{width: '10vw', bgcolor: '#fff'}}
            value={filterBy}
            onChange={handleChange}
            displayEmpty
            inputProps={{'aria-label': 'Without label'}}>
            <MenuItem value="origin">Origin</MenuItem>
            <MenuItem value="destination">Destination</MenuItem>
            <MenuItem value="cargoType">Cargo Type</MenuItem>
            <MenuItem value="weight">Weight</MenuItem>
            <MenuItem value="departure">Departure</MenuItem>
            <MenuItem value="arrival">
              {arrivalFieldLabelMap[parcelsColumnType]}
            </MenuItem>
          </Select>
        </div>
        <div>
          {['departure', 'arrival'].includes(filterBy) ? (
            <LocalizationProvider dateAdapter={AdapterMoment}>
              <DatePicker
                value={filterDatetimeValue}
                onChange={handleDateValueChange}
                renderInput={params => <TextField {...params} />}
                sx={{
                  width: '20vw',
                  bgcolor: '#fff',
                  mt: '33px',
                  borderRadius: '4px',
                }}
              />
            </LocalizationProvider>
          ) : (
            <Autocomplete
              disablePortal
              id="combo-box-demo"
              options={filteringValues()}
              sx={{
                width: '20vw',
                bgcolor: '#fff',
                mt: '33px',
                borderRadius: '4px',
              }}
              value={filterValue}
              onChange={handleFilterValueChange}
              renderInput={params => <TextField {...params} />}
            />
          )}
        </div>
        <button
          type="button"
          className={classes.parcelColumnIconButton}
          onClick={filterParcels}>
          <SearchIcon sx={{color: '#fff', fontSize: 40, mt: '33px'}} />
        </button>
      </div>
    </FormControl>
  );

  return (
    <div className={classes.parcelsColumn}>
      <div className={classes.parcelsColumnRow}>
        <span className={classes.parcelColumnHeader}>
          {parcelsColumnHeaderMap[parcelsColumnType]}
        </span>
        <button
          type="button"
          className={classes.parcelColumnIconButton}
          onClick={toggleFilteringVisible}>
          {!filteringVisible && (
            <FilterAltIcon sx={{color: '#fff', fontSize: 40}} />
          )}
          {filteringVisible && (
            <FilterAltOffIcon sx={{color: '#fff', fontSize: 40}} />
          )}
        </button>
      </div>
      {filteringVisible && <div>{createFilterForm()}</div>}
      <div
        className={`${classes.parcelColumnCardsContainer} ${
          filteringVisible ? classes.parcelColumnCardsContainerShort : ''
        }`}>
        {renderedParcelsList.map(sampleParcel => (
          <ParcelCard
            key={Math.floor(Math.random() * 1000)}
            parcelType={parcelsColumnType}
            cargoType={sampleParcel.cargoType}
            origin={sampleParcel.origin}
            destination={sampleParcel.destination}
            price={sampleParcel.price}
            depth={sampleParcel.depth}
            height={sampleParcel.height}
            bredth={sampleParcel.bredth}
            weight={sampleParcel.weight}
            departure={sampleParcel.departure}
            arrival={sampleParcel.arrival}
          />
        ))}
      </div>
    </div>
  );
};

ParcelsColumn.propTypes = {
  parcelsColumnType: PropTypes.string.isRequired,
};

export default ParcelsColumn;
